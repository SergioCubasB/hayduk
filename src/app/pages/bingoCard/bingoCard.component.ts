import { Component, OnInit } from '@angular/core';
import { CardService } from './service/card.service';

@Component({
  selector: 'app-bingocard',
  templateUrl: './bingoCard.component.html',
  styleUrls: ['./bingoCard.component.scss']
})
export class BingoCardComponent implements OnInit {
  card$: any = [];
  stateLoader: boolean = true;
  isLoading: boolean = true;

  constructor(
    private cardService: CardService
  ) { }

  ngOnInit(): void {
    this.getCard();
  }

  getCard(){
    this.cardService.getCardUser()
    .subscribe(
      (card:any) => {
        const { data } = card.data;
        this.card$ = data;
        
        this.isLoading = false;
      }
    )
  }

}
