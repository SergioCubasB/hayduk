export interface Country {
    name: any,
    capital: any,
    capitalInfo: any,
    continents: any,
    flags: any,
    maps: any,
    timezones: any,
    region: string
}
