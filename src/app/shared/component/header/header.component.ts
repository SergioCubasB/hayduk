import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorageService } from 'src/app/auth/service/local-storage.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public getScreenWidth: any;

  menu: boolean = true;
  isLoggin$:boolean = false;

  constructor(
    private localStorageService: LocalStorageService,
    private route: Router
  ) {
    this.getScreenWidth = window.innerWidth;
   }

  ngOnInit(): void {
    this.isLoggin();
  }

  toggleMenu(){
    this.menu = !this.menu;
  }

  openModalSession(){
    Swal.fire({
      title: 'Cerrar sesión',
      text: '¿Está seguro que deseas cerrar sesión?',
      width: 600,
      position: 'top-end',
      showDenyButton: true,
      showConfirmButton: false,
      showCancelButton: true,
      denyButtonText: `Si, Cerrar Sesión`,
      cancelButtonText: 'No, mantenerme aquí'
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isDenied) {
        this.localStorageService.remove("_TK");
        this.route.navigateByUrl("login");
      }else{
        Swal.close();
      }
    })

  }

  isLoggin(){
    const getSession = this.localStorageService.get("_TK");
    getSession.status === 200 ? this.isLoggin$ = true : this.isLoggin$ = false;
  }

}
